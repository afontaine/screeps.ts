declare interface HashMap<T> {
  [k: string]: T
}