/// <reference path="./constants.d.ts" />
/// <reference path="./common.d.ts" />


declare namespace Game {
  interface CPU {
    /**
     * @description Your CPU limit depending on your Global Control Level
     * @type {number}
     * @memberOf CPU
     */
    readonly limit: number;
    /**
     * @description An amount of available CPU time at the current game tick.
     * It can be higher than `Game.cpu.limit`.
     * @type {number}
     * @memberOf CPU
     */
    readonly tickLimit: number;
    /**
     * @description An amount of unused CPU accumulated in your bucket.
     * @type {number}
     * @memberOf CPU
     */
    readonly bucket: number;
    /**
     * @description Get amount of CPU time used from the beginning of the current game tick.
     * Always returns 0 in the Simulation mode.
     * @memberOf CPU
     * @return {number} Returns currently used CPU time as a float number
     */
    getUsed(): number;
  }

  interface GCL {
    /**
     * @description The current level.
     * @type {number}
     * @memberOf GCL
     */
    readonly level: number;
    /**
     * @description The current progress to the next level.
     * @type {number}
     * @memberOf GCL
     */
    readonly progress: number;
    /**
     * The progress required to reach the next level.
     * @type {number}
     * @memberOf GCL
     */
    readonly progressTotal: number;
  }

  type Path = Array<{
    exit: FIND_EXIT_TOP | FIND_EXIT_BOTTOM | FIND_EXIT_RIGHT | FIND_EXIT_LEFT,
    room: string
  }>;
  /**
   * @description An object with the folling options.
   * @interface PathFindingOptions
   */
  interface PathFindingOptions {
    /**
     * @description Function used to calculate the cost of entering the room.
     * @param {string} roomName The name of the room to navigate to.
     * @param {string} fromRoomName The name of the room navigating from
     * @return {number} The cost of navigating to the room. `Infinity` blocks the room.
     * @memberOf PathFindingOptions
     */
    routeCallback(roomName?: string, fromRoomName?: string): number;
  }
  interface Map {
    /**
     * @description List all exits available from teh room with the given name.
     * @param {string} roomName The room name
     * @return {Exits|Null} The exits information in the following format, or
     * null if the room not found.
     * ```
     * {
     *   "1": "W8N4",    // TOP
     *   "3": "W7N3",    // RIGHT
     *   "5": "W8N2",    // BOTTOM
     *   "7": "W9N3"     // LEFT
     * } 
     * ```
     * @memberOf Map
     */
    describeExits(roomName: string): {
      EXIT_TOP?: string,
      EXIT_RIGHT?: string,
      EXIT_BOTTOM?: string,
      EXIT_LEFT?: string
    } | null;
    /**
     * @description Find the exit direction from the given room en route to another room.
     * @param {string|Room} fromRoom Start room name or room object
     * @param {string|Room} toRoom Finish room name or room object.
     * @param {PathFindingOptions} [opts] An object with the pathfinding options. See {@linkcode findRoute}.
     * @return {number} Constant describing direction or error.
     * @memberOf Map
     */
    findExit(
      fromRoom: string | Room,
      toRoom: string | Room,
      opts?: PathFindingOptions
    ): FIND_EXIT_TOP | FIND_EXIT_BOTTOM | FIND_EXIT_RIGHT | FIND_EXIT_LEFT | ERR_NO_PATH | ERR_INVALID_ARGS;
    /**
     * @description Find route from the given room to another room.
     * @param {string|Room} fromRoom Start room name or room object.
     * @param {string|Room} toRoom Finish room name or room object.
     * @param {PathFindingOptions} [opts] An object with a callback to calculate the cost. See {@link PathFindingOptions}
     * @return {Path} The path to the room.
     * @memberOf Map
     */
    findRoute(fromRoom: string | Room, toRoom: string | Room, opts?: PathFindingOptions): Path | ERR_NO_PATH;
    /**
     * @description Get the linear distance (in rooms) between two rooms.
     * @param {string} roomName1 The name of the first room.
     * @param {string} roomName2 The name of the second room.
     * @param {boolean} [continuous] Whether to treat the world map continuous
     * on borders. Set to true if you  want to calculate the trade or terminal
     * send cost. Default is false.
     * @returns {number} A number of rooms between the given two rooms.
     * 
     * @memberOf Map
     */
    getRoomLinearDistance(roomName1: string, roomName2: string, continuous?: boolean): number;
    /**
     * @description Get terrain type at the specified room position. This
     * method works for any room in the world even if you have no access
     * to it.
     * @param {number} x X position in the room.
     * @param {number} y Y position in the room.
     * @param {string} roomName The room name.
     * @returns {("plain" | "swamp" | "wall")} The terrain type.
     * @memberOf Map
     */
    getTerrainAt(x: number, y: number, roomName: string): "plain" | "swamp" | "wall";
    /**
     * @description Get terrain type at the specified room position. This
     * method works for any room in the world even if you have no access
     * to it.
     * @param {RoomPosition} pos The position object.
     * @returns {("plain" | "swamp" | "wall")} The terrain type.
     * @memberOf Map
     */
    getTerrainAt(pos: RoomPosition): "plain" | "swamp" | "wall";
    /**
     * @description Check if the room is available to move into.
     * @param {string} roomName The room name.
     * @returns {boolean} Whether or not the room is available to move into.
     * @memberOf Map
     */
    isRoomAvailable(roomName: string): boolean;
  }

  interface Market {
    /**
     * Your current credits balance.
     * @type {number}
     * @memberOf Market
     */
    readonly credits: number;
    /**
     * An array of the last 100 incoming transactions to your terminals.
     * @type {Array<Transaction>}
     * @memberOf Market
     */
    readonly incommingTransactions: Array<Transaction>;
    /**
     * An array of the last 100 outgoing transactions from your terminals.
     * 
     * @type {Array<Transaction>}
     * @memberOf Market
     */
    readonly outgoingTransactions: Array<Transaction>;
    /**
     * A hashmap with your active and inactive buy/sell orders on the market.
     * 
     * @type {HashMap<Order>}
     * @memberOf Market
     */
    readonly orders: HashMap<Order>;
    /**
     * @description Estimate the energy transaction cost.
     * @param {number} amount Amount of resources to be sent
     * @param {string} roomName1 The name of the first room.
     * @param {string} roomName2 The name of the second room.
     * @returns {number} The amount of energy required to perform the transaction.
     * 
     * @memberOf Market
     */
    calcTransactionCost(amount: number, roomName1: string, roomName2: string): number;
    /**
     * @description Cancel a previously created order. The 5% fee is not returned.
     * 
     * @param {string} orderId The order ID.
     * @returns {(OK | ERR_INVALID_ARGS)} Either OK or ERR_INVALID_ARGS.
     * 
     * @memberOf Market
     */
    cancelOrder(orderId: string): OK | ERR_INVALID_ARGS;
    /**
     * @description Change the price of an existing order.
     * @param {string} orderId The order ID.
     * @param {number} newPrice The new order price.
     * @returns {(OK | ERR_NOT_OWNER | ERR_NOT_ENOUGH_RESOURCE | ERR_INVALID_ARGS)}
     * One of the following: OK, ERR_NOT_OWNER, ERR_NOT_ENOUGH_RESOURCE, ERR_INVALID_ARGS
     * @memberOf Market
     */
    changeOrderPrice(orderId: string, newPrice: number): OK | ERR_NOT_OWNER | ERR_NOT_ENOUGH_RESOURCES | ERR_INVALID_ARGS
    /**
     * @description Create a market order in your terminal.
     * 
     * @param {(ORDER_BUY | ORDER_SELL)} type The order type, either buy or sell.
     * @param {(RESOURCES | SUBSCRIPTION_TOKEN)} resourceType The resource type or subscription token.
     * @param {number} price The price for one resource unit in credits.
     * @param {number} totalAmount The amount of resource to be traded in total
     * @param {string} [roomName] The room where your order will be created.
     * 
     * @memberOf Market
     */
    createOrder(
      type: ORDER_BUY | ORDER_SELL,
      resourceType: RESOURCES | SUBSCRIPTION_TOKEN,
      price: number,
      totalAmount: number,
      roomName?: string
    ): OK | ERR_NOT_OWNER | ERR_NOT_ENOUGH_RESOURCES | ERR_FULL | ERR_INVALID_ARGS;
    /**
     * @description Execute a trade deal from your terminal in `yourRoomName`
     * to another player's Terminal using the speicified buy/sell order.
     * @param {string} orderId The order ID.
     * @param {number} amount The amount of resources to transfer.
     * @param {string} [yourRoomName] The name of the room which has to contain an active Terminal.
     * @returns {(OK | ERR_NOT_OWNER | ERR_NOT_ENOUGH_RESOURCES | ERR_FULL | ERR_INVALID_ARGS)}
     * 
     * @memberOf Market
     */
    deal(
      orderId: string,
      amount: number,
      yourRoomName?: string
    ): OK | ERR_NOT_OWNER | ERR_NOT_ENOUGH_RESOURCES | ERR_FULL | ERR_INVALID_ARGS;
    /**
     * @description Add more capacity to an existing order.
     * @param {string} orderId The order ID.
     * @param {number} addAmount How much capacity to add. Cannot be negative.
     * @returns {(OK | ERR_NOT_ENOUGH_RESOURCES | ERR_INVALID_ARGS)}
     * 
     * @memberOf Market
     */
    extendOrder(orderId: string, addAmount: number): OK | ERR_NOT_ENOUGH_RESOURCES | ERR_INVALID_ARGS;
    /**
     * @description Get other players' orders currently active.
     * 
     * @param {(_.ListIterator<Order, boolean>|Object|Array<any>|string)} [filter] An
     * object or function that will filter the resulting list using the
     * {@link lodash.filter} method.
     * @returns {Array<Order>} The orders that match the filter.
     * 
     * @memberOf Market
     */
    getAllOrders(filter?: _.ListIterator<Order, boolean>|Object|Array<any>|string): Array<Order>;
    /**
     * @description Retrieve info for specific market order.
     * @param {string} id The order ID.
     * @returns {Order} An object with the order info.
     * 
     * @memberOf Market
     */
    getOrderById(id: string): Order;
  }
  interface Transaction {
    readonly transactionId: string;
    readonly time: number;
    readonly sender: User;
    readonly recipient: User;
    readonly resourceType: RESOURCES | SUBSCRIPTION_TOKEN;
    readonly amount: number
    readonly from: string;
    readonly to: string;
    readonly description: string;
    readonly order: {
      id: string;
      type: ORDER_BUY | ORDER_SELL;
      price: number
    }
  }
  interface Order {
    /**
     * @description The unique order ID
     * @type {string}
     * @memberOf Order
     */
    readonly id: string;
    /**
     * @description Whether this order is active and visible ot other players.
     * @type {boolean}
     * @memberOf Order
     */
    readonly active?: boolean;
    /**
     * @description The order creation time in game ticks
     * @type {number}
     * @memberOf Order
     */
    readonly created: number;
    /**
     * @description Whether this is a buy order or sell order.
     * @type {(ORDER_BUY | ORDER_SELL)}
     * @memberOf Order
     */
    readonly type: ORDER_BUY | ORDER_SELL;
    /**
     * @description Either one of the `RESOURCE_*` constants or `SUBSCRIPTION_TOKEN`.
     * @type {(RESOURCES | SUBSCRIPTION_TOKEN)}
     * @memberOf Order
     */
    readonly resourceType: RESOURCES | SUBSCRIPTION_TOKEN;
    /**
     * @description The room where this order is placed.
     * @type {string}
     * @memberOf Order
     */
    readonly roomName: string;
    /**
     * @description Currently available amount to trade.
     * @type {number}
     * @memberOf Order
     */
    readonly amount: number;
    /**
     * @description How many resources are left to trade via this order.
     * @type {number}
     * @memberOf Order
     */
    readonly remainingAmount: number;
    /**
     * @description Initial order amount.
     * @type {number}
     * @memberOf Order
     */
    readonly totalAmount?: number;
    /**
     * @description The price of one unit of the resource
     * @type {number}
     * @memberOf Order
     */
    readonly price: number;
  }
  interface User {
    readonly username: string;
  }
}

/**
 * @description The main global game object containing all the gameplay information.
 * 
 * @interface Game
 */
interface Game {
  /**
   * @description A hash containing all your construction sites with their id as hash keys.
   * @type {HashMap<ConstructionSite>}
   * @memberOf Game
   */
  readonly constructionsites: HashMap<ConstructionSite>;
  /**
   * @description An object containing information about your CPU usage.
   * @type {Game.CPU}
   * @memberOf Game
   */
  readonly cpu: Game.CPU;
  /**
   * @description A hash containing all your creeps with creep names as hash keys.
   * @type {HashMap<Creeps>}
   * @memberOf Game
   */
  readonly creeps: HashMap<Creeps>;
  /**
   * @description A hash containing all your flags with flag names as hash keys.
   * @type {HashMap<Flags>}
   * @memberOf Game
   */
  readonly flags: HashMap<Flags>;
  /**
   * @description Your Global Control Level
   * @type {Game.GCL}
   * @memberOf Game
   */
  readonly gcl: Game.GCL;
  /**
   * @description A global object representing world map.
   * @type {Map}
   * @memberOf Game
   */
  readonly map: Game.Map;
  /**
   * @description A global object representing the in-game market.
   * @type {Market}
   * @memberOf Game
   */
  readonly market: Game.Market;
  /**
   * @description An object with your global resources that are bound ot the account, like subscription tokens.
   * Each object key is a resource constant, values are resources amounts.
   * @type {HashMap<Resources>}
   * @memberOf Game
   */
  readonly resources: HashMap<Resources>;
  /**
   * @description description
   * @type {HashMap<Room>}
   * @memberOf Game
   */
  readonly rooms: HashMap<Room>;
  /**
   * @description A hash containing all your spawns with spawn names as hash keys.
   * @type {HashMap<StructureSpawn>}
   * @memberOf Game
   */
  readonly spawns: HashMap<StructureSpawn>;
  /**
   * @description A hash containing all your structures with Structure id as hash keys.
   * @type {HashMap<Structure>}
   * @memberOf Game
   */
  readonly structures: HashMap<Structure>;
  /**
   * @description System game tick counter. It is automatically incremented on every tick.
   * @type {number}
   * @memberOf Game
   */
  readonly time: number;

  /**
   * @description Get an object with the specified unique ID. It may be a game
   * obbject of any time. Only objects from the rooms which are visible to you
   * can be accessed.
   * @param {string} id The unique identifier
   * @return {T extends RoomObject} Returns an object insstance or null if it cannot be found.
   * @memberOf Game
   */
  getObjectById<T extends RoomObject>(id: string): T;
  /**
   * @description Send a custom message at your profile email. This way, you can
   * set up notifications to yourself on any occasion within the game. You can
   * schedule up to 20 notifications during one game tick. Not available in the
   * Simulation Room.
   * @param {string} message Custom text which will be sent in the message.
   * Maximum length is 1000 characters.
   * @param {number} groupInterval If set to 0 (default), the notification
   * will be scheduled immediately. Otherwise, it will be grouped with
   * other notifications and mailed out later using the specified time
   * in minutes.
   * @memberOf Game
   */
  notify(message: string, groupInterval?: number): void;
}

/**
 * @description The main global game object containing all the gameplay
 * information.
 */
declare const Game: Game;